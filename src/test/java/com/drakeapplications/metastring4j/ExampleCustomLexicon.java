
package com.drakeapplications.metastring4j;

import com.drakeapplications.metastring4j.MetaStringDefaultLexicon;

public class ExampleCustomLexicon extends MetaStringDefaultLexicon
{

    public ExampleCustomLexicon()
    {
      super();
    }

    @Override
    protected void define()
    {
      super.define();
      mStartTag = "{";
      mEndTag = "}";
    }
}
