/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringEvalException;

/**
 * This tag evaluates an Else condition.
 * @since 1.0
 */
public class ElseTag extends ConditionalTag implements NonParameterized
{
    /**
     * Construct an Else MetaStringTag
     * @since 1.0
     */
    public ElseTag()
    {
      super(true, true, false, null, null);
    }

    /**
     * Determines wither a tag condition evaluated by a command is
     * met or not in the current evaluation context.
     *
     * @since 1.0
     * @param inContext the evaluation context
     * @return true if the condition is met
     * @throws MetaStringEvalException
     */
    @Override
    protected boolean isConditionMet(MetaStringContext inContext) throws MetaStringEvalException
    {
      // Else always evaluates true
      return true;
    }
}

