/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

import java.util.List;

/**
 * Delegate interface, particularly for sub-MetaString
 * acquisition.
 *
 * @since 1.0
 */
public interface MetaStringDelegate
{
    /**
     * Get the MetaString object specified by the arguments.
     * @since 1.0
     *
     * @param inArguments list of args describing what MetaString to get
     */
    public MetaString getSubMetaString(List<String> inArguments);

    /**
     * Evaluate a non-standard comparison operation.
     * @since 1.0
     *
     * @param inCommand the non-standard operation
     * @param inContext the MetaString evaluation context
     * @param inArguments the conditional arguments
     * @return whether or not the condition is met
     * @throws MetaStringEvalException if the command in unknown to the delegate or evaluation fails
     */
    public boolean isCommandConditionMet(String inCommand, MetaStringContext inContext, List<String> inArguments) throws MetaStringEvalException;

}

