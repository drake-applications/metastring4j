/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

import java.util.Map;
import java.util.HashMap;

import java.lang.reflect.Method;

/**
 * This class defines the current state of the parameters
 * during a MetaString evaluation and provides access to
 * the delegate.
 * 
 * @since 1.0
 */
public class MetaStringContext
{
    private Map<String, Object> mParams = null;
    private Map<String, Object> mOverrides = null;
    private Map<String, Object> mEvalContext = null;
    private Map<String, Object> mDefaults = null;
    private MetaStringDelegate mDelegate = null;

    /**
     * Construct a MetaStringContext from supplied parameters,
     * optionally supplying a delegate.
     * @since 1.0
     *
     * @param inParams the regular parmeters driving the MetaString
     * @param inOverrides overridden parameters (take precedence)
     * @param inDelegate the delegate for sub-MetaStrings or null
     */
    public MetaStringContext(Map<String, Object> inParams,
                             Map<String, Object> inOverrides,
                             MetaStringDelegate inDelegate)
    {
      mParams = inParams;
      mOverrides = inOverrides;
      mDelegate = inDelegate;
      mEvalContext = new HashMap<>();
      mDefaults = new HashMap<>();
    }

    /**
     * Determine whether the given parameter name exists in the
     * context in any of the parameter containers.
     * @since 1.0
     *
     * @param inParamName the name of the parameter to find
     * @return whether the parameter was found or not
     */
    public boolean hasValue(String inParamName)
    {
      String name = null;
      String[] parts = inParamName.split("\\.");

      if (parts.length == 0)
      {
        name = inParamName;
      }
      else
      {
        name = parts[0];
      }

      return mEvalContext.containsKey(name) ||
             mOverrides.containsKey(name) ||
             mParams.containsKey(name) ||
             mDefaults.containsKey(inParamName);  // This is correct
    }

    /**
     * Determine whether any defined parameter in the context
     * matches the given the given regular expression.
     * @since 1.0
     *
     * @param inRegex regular expression to match
     * @return whether a matching parameter was found or not
     */
    public boolean hasMatchingValue(String inRegex)
    {
      return mapHasMatchingKey(mEvalContext, inRegex) ||
             mapHasMatchingKey(mOverrides, inRegex) ||
             mapHasMatchingKey(mParams, inRegex) ||
             mapHasMatchingKey(mDefaults, inRegex);
    }

    /**
     * Internal function to assist in finding a parameter within
     * a particluar Map that matches the given regular expression.
     * @since 1.0
     *
     * @param inMap the map of parameters to search
     * @param inRegex the regular expression to match
     * @return whether a regular expresison match was found in the map
     */
    private boolean mapHasMatchingKey(Map<String, Object> inMap, String inRegex)
    {
      boolean hasMatch = false;
      for (String key : inMap.keySet())
      {
        hasMatch = key.matches(inRegex);
        if (hasMatch)
          break;
      }
      return hasMatch;
    }

    /**
     * Get the value of a named parameter, looking in the context
     * maps by order of precedence: evaluation context, overrides,
     * normal parameters, and defaults.
     * @since 1.0
     *
     * @param inParamName the name of the parameter to find
     * @return the value of the parameter in the current context
     * @throws MetaStringEvalException
     */
    public Object getValue(String inParamName) throws MetaStringEvalException
    {
      boolean usedDefault = false;
      Object obj = null;
      String name = null;

      String[] parts = inParamName.split("\\.");
      if (parts.length == 0)
      {
        name = inParamName;
      }
      else
      {
        name = parts[0];
      }

      if (mEvalContext.containsKey(name))
      {
        obj = mEvalContext.get(name);
      }
      else if (mOverrides.containsKey(name))
      {
        obj = mOverrides.get(name);
      }
      else if (mParams.containsKey(name))
      {
        obj = mParams.get(name);
      }
      else if (mDefaults.containsKey(inParamName))
      {
        obj = mDefaults.get(inParamName);
        usedDefault = true;
      }

      if (!usedDefault && parts.length > 1)
      {
        obj = getObjectProperty(obj, parts, 1);
      }

      return obj;
    }

    /**
     * Use reflection to get the value of a named property from the
     * given object.
     * @since 1.0
     * @param inObject the object to reflect
     * @param inPath the property path
     * @param inIndex where in the path to get the next part
     * @return the value of the property on the object
     * @throws MetaStringEvalException
     */
    protected Object getObjectProperty(Object inObj, String[] inPath, int inIndex) throws MetaStringEvalException
    {
      Object object = null;
      String propName = null;
      String methName = null;

      try
      {
        propName = inPath[inIndex];
        methName = propName.substring(0, 1).toUpperCase() + propName.substring(1);
        Method method = inObj.getClass().getMethod("get" + methName);
        object = method.invoke(inObj);
      }
      catch (Exception ex)
      {
        throw new MetaStringEvalException("Failed to obtain value for property '" + propName + "'", ex);
      }

      inIndex = inIndex + 1;
      if (inPath.length > inIndex)
      {
        object = getObjectProperty(object, inPath, inIndex);
      }
      return object;
    }

    /**
     * Set a value in the evaluation context (highest priority).
     * @since 1.0
     *
     * @param inName the name of the variable
     * @param inObject the value of the variable
     */
    public void setVariable(String inName, Object inObject)
    {
      mEvalContext.put(inName, inObject);
    }

    /**
     * Remove a named variable from the evaluation context.
     * @since 1.0
     *
     * @param inName the name of the variable to remove
     */
    public void removeVariable(String inName)
    {
      mEvalContext.remove(inName);
    }

    /**
     * Set a default value for a named parameter, if no value
     * is supplied.
     * @since 1.0
     *
     * @param inName name of the default parameter
     * @param inObject value of the default parameter
     */
    public void setDefault(String inName, Object inObject)
    {
      Object obj = inObject;
      if (obj == null)
      {
        obj = "";
      }
      mDefaults.put(inName, obj);
    }

    /**
     * Remove a named default parameter.
     * @since 1.0
     *
     * @param inName name o the default parameter to remove
     */
    public void removeDefault(String inName)
    {
      mDefaults.remove(inName);
    }

    /**
     * Check whether a delegate is defined in this context.
     * @since 1.0
     *
     * @return true if delegate is set
     */
    public boolean hasDelegate()
    {
      return mDelegate != null;
    }

    /**
     * Get the delegate assigned to this context.
     * @since 1.0
     *
     * @return the delegate (or null)
     */
    public MetaStringDelegate getDelegate()
    {
      return mDelegate;
    }
}

