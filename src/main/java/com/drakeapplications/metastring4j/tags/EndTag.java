/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

/**
 * An end tag denotes the end of the current control structure.
 * @since 1.0
 */
public class EndTag extends ControlTag implements PairedControlEnder, NonParameterized
{
    protected String mType = null;

    /**
     * Construct an empty EndTag.
     */
    public EndTag()
    {
      super(false, true, null);
    }

    /**
     * Construct an EndTag.
     * @since 1.0
     *
     * @param inType the type of end tag
     */
    public EndTag(String inType)
    {
      super(false, true, null);
      mType = inType;
    }

    /**
     * Get the type of EndTag.
     * @since 1.0
     * @return the type of EndTag
     */
    public String getType()
    {
      return mType;
    }

    /**
     * Set the type of EndTag.
     * @since 1.0
     * @param inType the name of the type
     */
    public void setType(String inType)
    {
      mType = inType;
    }
}

