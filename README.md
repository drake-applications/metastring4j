# MetaString4J
## About
This is a Java implementation of the MMetaString class from MPlant core, which in turn, is a completely reinvented implementation of the MetaSQL concept used by the OpenRPT reporting engine.

### What is a Meta String?
A Meta String is a parameterized string capable of being morphed into many different resulting strings based on the parameters supplied.  Perhaps the most obvious or helpful use of Meta Strings is with SQL.  Meta Strings reduce the number of SQL statements needed to perform a variety of similar, though different queries and eliminates the need for programs to build SQL statements piecemeal.  See the SQL example below.

Another use for Meta Strings is in templating, particluarly where there are conditions on whether a block of output is desired or if there is a need for repeated output.  See the HTML example below.

### Improvements
MetaString4J and MMetaString (the MPlant C++ version) have additional features not supported by MetaSQL, such as user-defined lexicons and smart list evaluation (list objects automatically expand in a value statement).

In porting from MMetaString, we have done our best to maintain Java principles and concepts.  For example, Java's Object class completely obviates the need for the MParam C++ class with respect to Meta Strings.

MetaString4J also adds features beyond what is currently in the MPlant version.  These include:
- If conditions beyond a simple exists or does not exist, e.g. `<? if('item > 1') ?>`, though compound logic is not yet supported.
- Multiple simultaneous iterators in foreach statements, e.g. `<? foreach('x, y') ?>`.
- Delimited foreach statements
- Sub-MetaStrings or placeholders where a delegate can load another MetaString from another location.
- Default values for parameters not supplied with the default tag.
- Multiple values in if statements, such as `<? if exists('arg1, arg2') ?>` or `<? if any('arg1', 'arg2') ?>`.

## Examples
The following examples use the default lexicon, but each of the key words and all of the syntax can be changed by defining a custom lexicon.

Meta Strings generally work well for problems where you are trying to build executable strings, such as SQL. For simple string formatting, Meta Strings are overkill.

### SQL
The following example shows how one Meta String can reduce the amount of SQL you have to write:
```
  SELECT id,
      <? if exists("details") ?>
         first_name,
         last_name,
         start_date
      <? endif ?>
         full_name,
         emp_number
    FROM employee
   WHERE 1 = 1
    <? if exists("id") ?>
     AND id = <? value("id") ?>
    <? elseif exists("number") ?>
     AND emp_number = <? value("number") ?>
    <? endif ?>
    <? if exists("before_start_date") ?>
     AND start_date < <? value("start_date") ?>
    <? endif ?>
```
As we can see, the above code by itself can replace eight individual queries:

- Get employee by id
- Get employee by employee number
- Get employees who started before a given date
- Get a list of all employees
- Get detailed employee info by id
- Get detailed employee info by employee number
- Get detailed employee info for employees who started before a given date.
- Get a detailed list of all employees

The resulting query depends on what parameters are supplied to the Meta String. In the following snippet, we get query to obtain details for one employee:

```
HashMap<String, Object> params = new HashMap<String, Object>();
params.put("details", Integer.valueOf(1));
params.put("number", inEmployeeNumber);

MetaStringBuilder builder = new MetaStringBuilder()
                                .formatter(new MetaStringSqlFormatter());

MetaString meta = MetaStringBuilder.buildMetaString();
meta.setString(...); // Here we would load the string above from a file, etc.
return meta.evaluate(params);
```
The MetaString becomes more useful when, for example, you have a UI and you set your params based on the inputs given (or not given) by the user.  Then you would have one function that accepts that parameter map, produces/executes the Meta String, etc. In such a situation the amount of code to write is greatly reduced.

### HTML
This somewhat less interesting example dynamically builds a page based on parameters.

```
<html>
<body>
  <? if exists("employee") ?>
  <h3><? value("employee") ?></h3>
  <table>
    <tr>
      <td class="heading">Start Date</td>
      <td><? value("start_date") ?></td>
    </tr>
  </table>
  <h4>What would you like to do?</h4>
  <ul>
    <? foreach("route, command") ?>
       <li><a href="route/<? value('route') ?>"><? value("command") ?></a></li>
    <? endforeach ?>
  </ul>
  <? else ?>
    <p>Sorry, you are not an employee.</p>
  <? endif ?>
</body>
</html>
```

### Trivial example
```
String buildUserState(Map<String, Object> inParams)
{
  String result = null;

  // Assume we defined a MetaStringBuilder member variable earlier

  MetaString meta = myBuilder.buildMetaString();
  StringBuilder strBuilder = new StringBuilder();

  strBuilder.append("User <? value('user') ?> ")
            .append("<? if exists('dead') ?>has died")
            .append("<? else ?>is still alive")
            .append(".<? endif ?>");
  try
  {
    meta.setString(strBuilder.toString());
    result = meta.evaluate(inParams);
  }
  catch (Exception e)
  {
    // A parse or eval error occurred
    result = null;
  }
  return result;
}  
```

## What about SQL injection?
The MetaStringSqlFormatter class handles basic string escaping for parameters supplied to a Meta String for the purpose of writing SQL statements.  Parameters that evaulate as strings are surrounded by single quotes, and embedded single quotes are doubled (' becomes '').  You may also supply your own MetaStringFormatter subclass to override this (and other) behaviors depending on your needs.

MetaString4J attempts to be a stand-alone library, so it does not bind to any specific SQL language or driver.  However, if you would like to use a driver's functions for preventing SQL injection, you can do so by subclassing MetaStringFormatter, implementing the escapeString function, and calling the more robust string escape from the driver.

## License
MetaString4J is copyright 2021 Drake Applications and is licensed under the Apache 2.0 license.  See the LICENSE file for details.

## History
The original MetaString parser from which this project is based was written in Objective-C while I was at Paladin Logic.  It was used in several iOS Apps.  I later ported it to VisualBasic for another Paladin project.  I received express permission to port the parser to C++ and again to Java, crediting Paladin Logic as a contributor.  I have improved the parser with each port.
