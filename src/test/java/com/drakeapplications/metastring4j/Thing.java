
package com.drakeapplications.metastring4j;

    public class Thing
    {
       private String mOne = null;
       private String mTwo = null;

       public Thing()
       {
         mOne = "one";
         mTwo = "two";
       }
       public String getOne()
       {
         return mOne;
       }
       public String getTwo()
       {
         return mTwo;
       }
    }
