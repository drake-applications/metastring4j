/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.List;

import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringEvalException;
import com.drakeapplications.metastring4j.MetaStringFormatter;

/**
 * An Evaluation tag that writes a formatted, parmeterized
 * value into the current MetaString.
 * @since 1.0
 */
public class ValueTag extends EvaluationTag
{
    /**
     * Construct an empty ValueTag.
     * @since 1.0
     */
    public ValueTag()
    {
      super(false, false, null);
    }

    /**
     * Construct a ValueTag.
     * @since 1.0
     *
     * @param inArguments zero or more inputs to the tag/command
     */
    public ValueTag(List<String> inArguments)
    {
      super(false, false, inArguments);
    }

    /**
     * Evaluate this tag, appending the result into the StringBuilder.
     * @since 1.0
     *
     * @param ioBuilder the string builder containing the result string
     * @param inContext the evaluation/parameter context
     * @param inFormatter the string formatter for the output
     * @return whether an evaluation was performed or not
     *
     */
    @Override
    public boolean evaluate(StringBuilder ioBuilder,
                            MetaStringContext inContext,
                            MetaStringFormatter inFormatter)
                   throws MetaStringEvalException
    {
      boolean evaluated = true;
      if (inContext.hasValue(mArguments.get(0)))
      {
        ioBuilder.append(evaluateParam(inContext.getValue(mArguments.get(0)), inFormatter));
      }
      else
      {
        ioBuilder.append(inFormatter.getNullValue());
      }
      return evaluated;
    }
}

