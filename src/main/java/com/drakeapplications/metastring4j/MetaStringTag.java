/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Internally used class representing a tag within a
 * MetaString that can be evaluated to modify the
 * resulting string.
 * @since 1.0
 */
public abstract class MetaStringTag
{
    protected String mName = null;

    protected boolean mStartOfBlock = false;
    protected boolean mEndOfBlock = false;

    protected List<String> mArguments = null;
    protected List<MetaStringBlock> mBlocks = null;

    /**
     * Construct a MetaStringTag.
     * @since 1.0
     *
     * @param inStart whether the tag is the start of a block
     * @param inEnd whether the tag is the end of a block
     * @param inParameters zero or more inputs to the tag/command
     */
    protected MetaStringTag(boolean inStart,
                            boolean inEnd,
                            List<String> inParameters)
    {
      if (inParameters == null)
      {
        inParameters = new ArrayList<>();
      }
      mArguments = inParameters;
      mStartOfBlock = inStart;
      mEndOfBlock = inEnd;

      mBlocks = new ArrayList<>();
    }

    /**
     * Get the name of the tag.
     * @since 1.0
     * @returns the name of the tag
     */
    public String getName()
    {
      return mName;
    }

    /**
     * Set the name of the tag.
     * @since 1.0
     * @param inName the tag name
     */
    public void setName(String inName)
    {
      mName = inName;
    }

    /**
     * Set the tag arguments.
     * @since 1.0
     * @param inArguments the tag arguments
     */
    public void setArguments(List<String> inArguments)
    {
      mArguments = inArguments;
    }

    /**
     * Validate that the MetaStringTag has been properly constructed.
     * @since 1.0
     * @param outMessage validation error message, if errors
     * @return true if valid
     */
    public boolean validate(StringBuilder outMessage)
    {
      // Subclasses should override and throw a MetaStringParseError
      // if the current state of the tag is invalid.
      return true;
    }

    /**
     * Does this tag start a block?
     * @since 1.0
     * @return true if the tag starts a block
     */
    public boolean isStartOfBlock()
    {
      return mStartOfBlock;
    }

    /**
     * Does this tag end a block?
     * @since 1.0
     * @return true if the tag ends a block
     */
    public boolean isEndOfBlock()
    {
      return mEndOfBlock;
    }

    /**
     * Add a block subordinate to this tag.
     * @since 1.0
     * @param inBlock the subordinate block
     */
    public void addBlock(MetaStringBlock inBlock)
    {
      mBlocks.add(inBlock);
    }

    /**
     * Evaluate this tag, appending the result into the StringBuilder.
     * @since 1.0
     *
     * @param ioBuilder the string builder containing the result string
     * @param inContext the evaluation/parameter context
     * @param inFormatter the string formatter for the output
     * @return whether an evaluation was performed or not
     */
    public boolean evaluate(StringBuilder ioBuilder,
                            MetaStringContext inContext,
                            MetaStringFormatter inFormatter)
    {
      return false;
    }

    /**
     * Internal method that determines whether a String value is
     * better interpreted as a Number or as a String.
     *
     * @since 1.0
     * @param inValue the value whose type will be deduced
     * @return an object of the deduced type
     */
    protected Object deduceObject(String inValue)
    {
      Object finalValue = inValue;

      if (inValue != null)
      {
        try
        {
          float dVal = Float.parseFloat(inValue);
          if (dVal == Math.floor(dVal) && !Float.isInfinite(dVal))
          {
            finalValue = Integer.valueOf(Math.round(dVal));
          }
          else
          {
            finalValue = Float.valueOf(dVal);
          }
        }
        catch (NumberFormatException ex)
        {
          // Ignore. The value is not numeric. We'll use the String as is.
        }
      }

      return finalValue;
    }
}

