/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.List;

import com.drakeapplications.metastring4j.MetaString;
import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringFormatter;
import com.drakeapplications.metastring4j.MetaStringEvalException;

/**
 * A ControlTag that allows a delegate to substitute
 * another MetaString into the current MetaString
 * being evaluated.
 * @since 1.0
 */
public class SubTag extends ControlTag
{
    /**
     * Construct an empty SubTag.
     * @since 1.0
     */
    public SubTag()
    {
      super(false, false, null);
    }

    /**
     * Construct a SubTag.
     * @since 1.0
     *
     * @param inArguments zero or more inputs to the tag/command
     */
    public SubTag(List<String> inArguments)
    {
      super(false, false, inArguments);
    }

    /**
     * Evaluate this tag, appending the result into the StringBuilder.
     * @since 1.0
     *
     * @param ioBuilder the string builder containing the result string
     * @param inContext the evaluation/parameter context
     * @param inFormatter the string formatter for the output
     * @return whether an evaluation was performed or not
     *
     */
    @Override
    public boolean evaluate(StringBuilder ioBuilder,
                            MetaStringContext inContext,
                            MetaStringFormatter inFormatter)
                   throws MetaStringEvalException
    {
      boolean evaluated = true;

      if (!inContext.hasDelegate())
      {
        throw new MetaStringEvalException("No delegate.  Cannot load sub MetaStrings.", null);
      }
      MetaString subMetaString = inContext.getDelegate().getSubMetaString(mArguments);
      if (subMetaString == null)
      {
        throw new MetaStringEvalException("Failed to load sub MetaString '" + mArguments.toString() + "'", null);
      }
      ioBuilder.append(subMetaString.evaluate(inContext));

      return evaluated;
    }
}

