/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

import java.util.HashMap;

import com.drakeapplications.metastring4j.commands.AnyCommand;
import com.drakeapplications.metastring4j.commands.DelegatedCommand;
import com.drakeapplications.metastring4j.commands.ExistsCommand;
import com.drakeapplications.metastring4j.commands.RegexExistsCommand;


import com.drakeapplications.metastring4j.tags.CommentTag;
import com.drakeapplications.metastring4j.tags.DefaultTag;
import com.drakeapplications.metastring4j.tags.ElseIfTag;
import com.drakeapplications.metastring4j.tags.ElseTag;
import com.drakeapplications.metastring4j.tags.EndTag;
import com.drakeapplications.metastring4j.tags.ForallTag;
import com.drakeapplications.metastring4j.tags.ForeachTag;
import com.drakeapplications.metastring4j.tags.IfTag;
import com.drakeapplications.metastring4j.tags.LiteralTag;
import com.drakeapplications.metastring4j.tags.SubTag;
import com.drakeapplications.metastring4j.tags.ValueTag;

/**
 * MetaStringLexicon
 *
 * This class defines the operators and key words for a
 * meta string.  You may subclass the lexicon to implement
 * your own syntax.  This abstract class defines all
 * entries as null.
 *
 * @since 1.0
 */
public abstract class MetaStringLexicon
{
    protected String mStartTag = null;
    protected String mEndTag = null;
    protected String mStartFunction = null;
    protected String mEndFunction = null;
    protected String mQuote = null;
    protected String mAltQuote = null;
    protected String mIf = null;
    protected String mElse = null;
    protected String mElseIf = null;
    protected String mEnd = null;
    protected String mNot = null;
    protected String mAny = null;
    protected String mExists = null;
    protected String mRegexExists = null;
    protected String mLiteral = null;
    protected String mValue = null;
    protected String mForeach = null;
    protected String mForall = null;
    protected String mDefault = null;
    protected String mSubMeta = null;
    protected String mDelimited = null;
    protected String mComment = null;

    private HashMap<String, Class<? extends MetaStringTag>> mTagWords = null;
    private HashMap<String, Class<? extends MetaStringCommand>> mCommandWords = null;

    /**
     * Default constructor defines all entries in
     * the lexicon as null.
     * @since 1.0
     */
    protected MetaStringLexicon()
    {
      define();
      initialize();
    }

    /**
     * Define the syntax of the lexicon.
     * @since 1.0
     */
    protected void define()
    {
      // Subclasses should override in order to
      // define the lexicon syntax.

      mStartTag = null;
      mEndTag = null;
      mStartFunction = null;
      mEndFunction = null;
      mQuote = null;
      mAltQuote = null;

      mIf = null;
      mElse = null;
      mElseIf = null;
      mEnd = null;
      mNot = null;
      mLiteral = null;
      mValue = null;
      mForeach = null;
      mForall = null;
      mComment = null;
      mDefault = null;
      mSubMeta = null;
      mDelimited = null;

      mAny = null;
      mExists = null;
      mRegexExists = null;
    }

    /**
     * Initialize the lexicon to be able to instantiate objects
     * associated with keywords.
     * @since 1.0
     */
    protected void initialize()
    {
      mTagWords = new HashMap<>();

      if (mIf != null)
      {
        mTagWords.put(mIf, IfTag.class);
      }
      if (mElse != null)
      {
        mTagWords.put(mElse, ElseTag.class);
      }
      if (mElseIf != null)
      {
        mTagWords.put(mElseIf, ElseIfTag.class);
      }
      if (mEnd != null)
      {
        mTagWords.put(mEnd, EndTag.class);
      }
      if (mLiteral != null)
      {
        mTagWords.put(mLiteral, LiteralTag.class);
      }
      if (mValue != null)
      {
        mTagWords.put(mValue, ValueTag.class);
      }
      if (mForeach != null)
      {
        mTagWords.put(mForeach, ForeachTag.class);
      }
      if (mForall != null)
      {
        mTagWords.put(mForall, ForallTag.class);
      }
      if (mDefault != null)
      {
        mTagWords.put(mDefault, DefaultTag.class);
      }
      if (mSubMeta != null)
      {
        mTagWords.put(mSubMeta, SubTag.class);
      }
      if (mComment != null)
      {
        mTagWords.put(mComment, CommentTag.class);
      }

      mCommandWords = new HashMap<>();

      if (mAny != null)
      {
        mCommandWords.put(mAny, AnyCommand.class);
      }
      if (mExists != null)
      {
        mCommandWords.put(mExists, ExistsCommand.class);
      }
      if (mRegexExists != null)
      {
        mCommandWords.put(mRegexExists, RegexExistsCommand.class);
      }
    }

    /**
     * Instantiate a conditional command from the lexicon.
     * @since 1.0
     * @param inCommand name of the command to instantiate
     * @return the meta string command
     */
    public MetaStringCommand newCommand(String inCommand)
    {
      MetaStringCommand command = null;
      Class<? extends MetaStringCommand> cmdClass = null;

      if (mCommandWords.containsKey(inCommand))
      {
        cmdClass = mCommandWords.get(inCommand);
        try
        {
          command = cmdClass.getDeclaredConstructor().newInstance();
        }
        catch (Exception ex)
        {
          command = null;
        }
      }
      else
      {
        command = new DelegatedCommand(inCommand);
      }

      return command;
    }

    /**
     * Instantiate a tag from the lexicon.
     * @since 1.0
     * @param inTagName name of the tag to instantiate
     * @return the meta string tag
     */
    public MetaStringTag newTag(String inTagName)
    {
      MetaStringTag tag = null;
      Class<? extends MetaStringTag> tagClass = null;

      if (mTagWords.containsKey(inTagName))
      {
        tagClass = mTagWords.get(inTagName);
        try
        {
          tag = tagClass.getDeclaredConstructor().newInstance();
        }
        catch (Exception ex)
        {
          tag = null;
        }
      }
      if (tag != null)
      {
        tag.setName(inTagName);
      }

      return tag;
    }

    /**
     * Accessor for start tag.
     * @since 1.0
     * @return how to identify a start tag
     */
    public String getStartTag()
    {
      return mStartTag;
    }

    /**
     * Accessor for end tag.
     * @since 1.0
     * @return how to identify an end tag
     */
    public String getEndTag()
    {
      return mEndTag;
    }

    /**
     * Accessor for start of function.
     * @since 1.0
     * @return how to identify the start of a function
     */
    public String getStartFunction()
    {
      return mStartFunction;
    }

    /**
     * Accessor for end of function.
     * @since 1.0
     * @return how to identify the end of a function
     */
    public String getEndFunction()
    {
      return mEndFunction;
    }

    /**
     * Accessor for primary quote identifier.
     * @since 1.0
     * @return primary quote identifier
     */
    public String getQuote()
    {
      return mQuote;
    }

    /**
     * Accessor for alternate quote identifier.
     * @since 1.0
     * @return alternate quote identifier
     */
    public String getAltQuote()
    {
      return mAltQuote;
    }

    /**
     * Accessor for "if" keyword.
     * @since 1.0
     * @return "if" keyword
     */
    public String getIf()
    {
      return mIf;
    }

    /**
     * Accessor for "else" keyword.
     * @since 1.0
     * @return "else" keyword
     */
    public String getElse()
    {
      return mElse;
    }

    /**
     * Accessor for "elseif" keyword.
     * @since 1.0
     * @return "elseif" keyword
     */
    public String getElseIf()
    {
      return mElseIf;
    }

    /**
     * Accessor for "end" keyword.
     * @since 1.0
     * @return "end" keyword
     */
    public String getEnd()
    {
      return mEnd;
    }

    /**
     * Accessor for "not" keyword.
     * @since 1.0
     * @return "not" keyword
     */
    public String getNot()
    {
      return mNot;
    }

    /**
     * Accessor for "any" keyword.
     * @since 1.0
     * @return "any" keyword
     */
    public String getAny()
    {
      return mAny;
    }

    /**
     * Accessor for "exists" keyword.
     * @since 1.0
     * @return "exists" keyword
     */
    public String getExists()
    {
      return mExists;
    }

    /**
     * Accessor for "reExists" keyword.
     * @since 1.0
     * @return "reExists" keyword
     */
    public String getRegexExists()
    {
      return mRegexExists;
    }

    /**
     * Accessor for "literal" keyword.
     * @since 1.0
     * @return "literal" keyword
     */
    public String getLiteral()
    {
      return mLiteral;
    }

    /**
     * Accessor for "value" keyword.
     * @since 1.0
     * @return "value" keyword
     */
    public String getValue()
    {
      return mValue;
    }

    /**
     * Accessor for "foreach" keyword.
     * @since 1.0
     * @return "foreach" keyword
     */
    public String getForeach()
    {
      return mForeach;
    }

    /**
     * Accessor for "forall" keyword.
     * @since 1.0
     * @return "forall" keyword
     */
    public String getForall()
    {
      return mForall;
    }

    /**
     * Accessor for "default" keyword.
     * @since 1.0
     * @return "default" keyword
     */
    public String getDefault()
    {
      return mDefault;
    }

    /**
     * Accessor for "comment" keyword.
     * @since 1.0
     * @return "comment" keyword
     */
    public String getComment()
    {
      return mComment;
    }
    /**
     * Accessor for "submeta" keyword.
     * @since 1.0
     * @return "submeta" keyword
     */
    public String getSubMeta()
    {
      return mSubMeta;
    }

    /**
     * Accessor for "delimited" keyword.
     * @since 1.0
     * @return "delimited" keyword
     */
    public String getDelimited()
    {
      return mDelimited;
    }
}
