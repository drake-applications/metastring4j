/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.List;

import com.drakeapplications.metastring4j.MetaStringCommand;
import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringEvalException;
import com.drakeapplications.metastring4j.MetaStringFormatter;

/**
 * A type of ControlTag that evaluates a condition.
 * @since 1.0
 */
public abstract class ConditionalTag extends ControlTag
{
    protected MetaStringCommand mCommand = null;
    protected boolean mIsNegated = false;

    /**
     * Construct a ConditionalTag
     * @since 1.0
     *
     * @param inStart whether the tag is the start of a block
     * @param inEnd whether the tag is the end of a block
     * @param inNegated whether the condition is negated (e.g. if not)
     * @param inCommand an operation to perform on tag parameters
     * @param inArguments zero or more inputs to the tag/command
     */
    protected ConditionalTag(boolean inStart,
                             boolean inEnd,
                             boolean inNegated,
                             MetaStringCommand inCommand,
                             List<String> inArguments)
    {
      super(inStart, inEnd, inArguments);
      mCommand = inCommand;
      mIsNegated = inNegated;
    }

    /**
     * Set whether the condition is negated or not.
     * @since 1.0
     * @param inNegated true to negate, false otherwise
     */
    public void setNegated(boolean inNegated)
    {
      mIsNegated = inNegated;
    }

    /**
     * Set the conditional command.
     * @since 1.0
     * @param inCommand the conditional command
     */
    public void setCommand(MetaStringCommand inCommand)
    {
      mCommand = inCommand;
    }

    /**
     * Evaluate this tag, appending the result into the StringBuilder.
     * @since 1.0
     *
     * @param ioBuilder the string builder containing the result string
     * @param inContext the evaluation/parameter context
     * @param inFormatter the string formatter for the output
     * @return whether an evaluation was performed or not
     */
    @Override
    public boolean evaluate(StringBuilder ioBuilder,
                            MetaStringContext inContext,
                            MetaStringFormatter inFormatter)
    {
      boolean evaluated = false;
      boolean conditionMet = isConditionMet(inContext);

      if (mIsNegated)
      {
        conditionMet = !conditionMet;
      }

      if (conditionMet)
      {
        evaluated = includeBlocks(ioBuilder, inContext, inFormatter);
      }
      return evaluated;
    }

    /**
     * Determines wither a tag condition evaluated by a command is
     * met or not in the current evaluation context.
     *
     * @since 1.0
     * @param inContext the evaluation context
     * @return true if the condition is met
     * @throws MetaStringEvalException
     */
    protected boolean isConditionMet(MetaStringContext inContext) throws MetaStringEvalException
    {
      boolean met = false;

      if (mCommand != null)
      {
        met = mCommand.isCommandConditionMet(inContext, mArguments);
      }
      else
      {
        met = isComparisonConditionMet(inContext);
      }
      return met;
    }

    /**
     * Determines wither a tag condition evaluated as a comparison is
     * met or not in the current evaluation context.
     *
     * @since 1.0
     * @param inContext the evaluation context
     * @return true if the condition is met
     * @throws MetaStringEvalException
     */
    protected boolean isComparisonConditionMet(MetaStringContext inContext) throws MetaStringEvalException
    {
      boolean met = false;

      // Expecting 3 args.
      if (mArguments.size() != 3)
      {
        throw new MetaStringEvalException("Incorrect number of arguments for generic comparison.  Expected 3, got " + mArguments.toString(), null);
      }

      String leftArg = mArguments.get(0);
      String operator = mArguments.get(1);
      String rightArg = mArguments.get(2);

      Object leftArgVal = evaluateArg(inContext, leftArg);
      Object rightArgVal = evaluateArg(inContext, rightArg);

      if (leftArgVal instanceof Comparable &&
          rightArgVal instanceof Comparable)
      {
        Comparable compLeftArg = (Comparable)leftArgVal;
        Comparable compRightArg = (Comparable)rightArgVal;
        try
        {
          met = evaluateCondition(compLeftArg, operator, compRightArg);
        }
        catch (Exception ex)
        {
          throw new MetaStringEvalException("Cannot compare incompatible objects.", ex);
        }
      }
      else
      {
        throw new MetaStringEvalException("Cannot compare incomparable objects.", null);
      }
      return met; 
    }

    /**
     * Determines wither a tag condition evaluated as a comparison is
     * met or not in the current evaluation context.
     *
     * @since 1.0
     * @param inContext the evaluation context
     * @param inArg a comparison argument
     * @return the evaluated argument
     * @throws MetaStringEvalException
     */
    protected Object evaluateArg(MetaStringContext inContext, String inArg) throws MetaStringEvalException
    {
      Object argVal;
      if (inContext.hasValue(inArg))
      {
        argVal = inContext.getValue(inArg);
      }
      else
      {
        argVal = deduceObject(inArg);
      }
      return argVal;
    }

    /**
     * Determine if a comparison between two Comparable values
     * results in a true evaluation, thus meeting the condition
     * of the tag.
     *
     * @since 1.0
     * @param inLeftValue the left operand
     * @param inOperator the comparison operation to perform
     * @param inRightValue the right operand
     * @return the result of the conditional evaluation
     * @throws MetaStringEvalException
     */
    protected boolean evaluateCondition(Comparable inLeftValue,
                                        String inOperator,
                                        Comparable inRightValue) throws MetaStringEvalException
    {
      boolean met = false;

      int result = inLeftValue.compareTo(inRightValue);
      if (inOperator.equals("=") || inOperator.equals("=="))
      {
        met = result == 0;
      }
      else if (inOperator.equals(">"))
      {
        met = result > 0;
      }
      else if (inOperator.equals("<"))
      {
        met = result < 0;
      }
      else if (inOperator.equals(">="))
      {
        met = result >= 0;
      }
      else if (inOperator.equals("<="))
      {
        met = result <= 0;
      }
      else if (inOperator.equals("!=") || inOperator.equals("<>"))
      {
        met = result != 0;
      }
      else
      {
        throw new MetaStringEvalException("Unknown operator '" + inOperator + "'", null);
      }

      return met;
    }
}

