/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.drakeapplications.metastring4j.MetaStringFormatter;
import com.drakeapplications.metastring4j.MetaStringTag;

/**
 * A type of MetaStringTag that creates a value in the
 * resulting string.
 * @since 1.0
 */
public abstract class EvaluationTag extends MetaStringTag
{
    /**
     * Construct a MetaStringTag.
     * @since 1.0
     *
     * @param inStart whether the tag is the start of a block
     * @param inEnd whether the tag is the end of a block
     * @param inArguments zero or more inputs to the tag/command
     */
    protected EvaluationTag(boolean inStart,
                            boolean inEnd,
                            List<String> inArguments)
    {
      super(inStart, inEnd, inArguments);
    }

    /**
     * @Inherit
     */
    @Override
    public boolean validate(StringBuilder outMessage)
    {
      boolean valid = true;
      if (mArguments.size() != 1)
      {
        outMessage.append("Incorrect number of arguments");
        valid = false;
      }
      return valid;
    }

    /**
     * Evaluate a parameter into the result String based on its
     * type and how the formatter treats it.
     *
     * @param inParam the object to format
     * @param inFormatter the MetaString formatter
     * @return String containing the written object
     */
    protected String evaluateParam(Object inParam, MetaStringFormatter inFormatter)
    {
      String evaluatedParam = "";

      if (inParam == null)
      {
        evaluatedParam = inFormatter.getNullValue();
      }
      else if (inParam instanceof String)
      {
        evaluatedParam = inFormatter.escapeString((String)inParam);
      }
      else if (inParam instanceof Number)
      {
        evaluatedParam = inParam.toString();
      }
      else if (inParam instanceof Collection)
      {
        Collection collection = (Collection)inParam;
        Iterator<Object> iter = collection.iterator();

        if (iter.hasNext())
        {
          StringBuilder builder = new StringBuilder();
          boolean isFirst = true;
          while (iter.hasNext())
          {
            Object object = iter.next();
            String value = null;

            if (object instanceof Number)
            {
              value = object.toString();
            }
            else
            {
              value = inFormatter.escapeString(object.toString());
            }

            if (!isFirst)
            {
              builder.append(inFormatter.getDelimiter());
            }
            else
            {
              isFirst = false;
            }
            builder.append(value);
          }
          evaluatedParam = builder.toString();
        }
        else
        {
          evaluatedParam = inFormatter.getNullValue();
        }
      }
      else
      {
        evaluatedParam = inFormatter.escapeString(inParam.toString());
      }

      return evaluatedParam;
    }
}

