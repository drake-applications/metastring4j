package com.drakeapplications.metastring4j;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import com.drakeapplications.metastring4j.MetaString;
import com.drakeapplications.metastring4j.MetaStringBuilder;
import com.drakeapplications.metastring4j.MetaStringDelegate;

import com.drakeapplications.metastring4j.Thing;

/**
 * Unit test for MetaString
 */
public class MetaStringSqlTest implements MetaStringDelegate
{
    private MetaStringBuilder mBuilder = null;
    private MetaString mMeta = null;
    private HashMap<String, Object> mParams = null;

    /**
     * Create the test case
     */
    public MetaStringSqlTest()
    {
      mBuilder = new MetaStringBuilder()
                     .setFormatter(new MetaStringSqlFormatter())
                     .setDelegate(this);

      mMeta = mBuilder.buildMetaString();
    }

    public MetaString getSubMetaString(List<String> inArgs)
    {
      MetaString subMeta = null;
      if (inArgs.get(0).equals("goofy"))
      {
        subMeta = mBuilder.buildMetaString();
        try
        {
          subMeta.setString("I am goofy <? value('yes') ?>");
        }
        catch (Exception ex)
        {
          System.out.println(ex.toString());
          subMeta = null;
        }
      }
      return subMeta;
    }

    public boolean isCommandConditionMet(String inCommand, MetaStringContext inContext, List<String> inArguments)
                     throws MetaStringEvalException
    {
      boolean met = false;
      if (inCommand.equals("known")) // Implement the 'if known' custom conditional command
      {
        met = inArguments.get(0).equals("goofy");
      }
      else
      {
        throw new MetaStringEvalException("Unknown command: " + inCommand, null);
      }
      return met;
    }

 
    @Before
    public void setup()
    {
      mParams = new HashMap<String, Object>();

      ArrayList<String> listOfStuff = new ArrayList<String>();
      listOfStuff.add("one");
      listOfStuff.add("two");
      listOfStuff.add("three");
      listOfStuff.add("four");

      ArrayList<Integer> pair = new ArrayList<Integer>();
      pair.add(1);
      pair.add(2);

      mParams.put("yes", Integer.valueOf(1));
      mParams.put("no", "N");
      mParams.put("stuff", listOfStuff);
      mParams.put("pair", pair);

      mParams.put("one", Integer.valueOf(1));
      mParams.put("two", Integer.valueOf(2));
      mParams.put("three", Integer.valueOf(3));
    }

    @After
    public void cleanup()
    {
      mParams = null;
    }

    @Test
    public void testIf1()
    {
      try 
      {
        mMeta.setString("Hello <? if exists('yes') ?>Joe<? else ?>Bob<? endif ?>!");
        assertEquals("Hello Joe!", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testIfNotExists()
    {
      try
      {
        mMeta.setString("<? if not exists('x') ?>Pass<? else ?>Fail<? endif ?>");
        assertEquals("Pass", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testNotEqual()
    {
      try
      {
        mMeta.setString("<? if('one != 1') ?>Fail<? else ?>Pass<? endif ?>");
        assertEquals("Pass", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testNestedIf()
    {
      try
      {
        mMeta.setString("<? if('one = 1') ?><? if('two = 2') ?>Pass<? endif ?><? endif ?>");
        assertEquals("Pass", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testIntValue()
    {
      try
      {
        mMeta.setString("<? value('yes') ?>");
        assertEquals("1", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testStringValue()
    {
      try
      {
        mMeta.setString("<? value('no') ?>");
        assertEquals("'N'", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testForeach()
    {
      try
      {
        mMeta.setString("<? foreach('stuff') ?>GOT <? value('stuff') ?><? endforeach ?>");
        assertEquals("GOT 'one'GOT 'two'GOT 'three'GOT 'four'", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testNestedForeach()
    {
      try
      {
        mMeta.setString("Hi <? foreach('stuff') ?><? foreach('pair') ?><? value('pair') ?> <? value('stuff') ?><? endforeach ?><? endforeach ?>");
        assertEquals("Hi 1 'one'2 'one'1 'two'2 'two'1 'three'2 'three'1 'four'2 'four'", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testDefaultValue()
    {
      try
      {
        mMeta.setString("<? default('x') ?>3<? enddefault ?><? value('x') ?>");
        String val = mMeta.evaluate(mParams);
        assertEquals("3", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testDefaultLiteral()
    {
      try
      {
        mMeta.setString("<? default('x') ?><? value('pair') ?><? enddefault ?><? literal('x') ?>");
        assertEquals("1, 2", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testEmptyForeach()
    {
      try
      {
        mMeta.setString("<? foreach('pair') ?><? endforeach ?>");
        assertEquals("", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testForeachDelimited()
    {
      try
      {
        mMeta.setString("<? foreach delimited('pair') ?>A<? endforeach ?>");
        assertEquals("A, A", mMeta.evaluate(mParams));
        mMeta.setString("<? foreach delimited ; ('pair') ?>A<? endforeach ?>");
        assertEquals("A;A", mMeta.evaluate(mParams));
        mMeta.setString("<? foreach delimited <br> ('pair') ?>A<? endforeach ?>");
        assertEquals("A<br>A", mMeta.evaluate(mParams));
        mMeta.setString("<? foreach delimited x x ('pair') ?>A<? endforeach ?>");
        assertEquals("Ax xA", mMeta.evaluate(mParams));
        mMeta.setString("<? foreach delimited ', ' ('pair') ?>A<? endforeach ?>");
        assertEquals("A, A", mMeta.evaluate(mParams));
        mMeta.setString("<? foreach delimited '', '' ('pair') ?>A<? endforeach ?>");
        assertEquals("A', 'A", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testMultiIteratorForeach()
    {
      try
      {
        mMeta.setString("<? foreach('pair, stuff') ?><? value('pair') ?> <?value('stuff') ?><? endforeach ?>");
        assertEquals("1 'one'2 'two'", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testMultiIteratorForall()
    {
      try
      {
        mMeta.setString("<? forall('pair, stuff') ?><? value('pair') ?> <?value('stuff') ?><? endforall ?>");
        assertEquals("1 'one'2 'two'null 'three'null 'four'", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }

    }

    @Test
    public void testSubMeta()
    {
      try
      {
        mMeta.setString("<? if known('goofy') ?><? sub('goofy') ?><? else ?>FAIL<? endif ?>");
        assertEquals("I am goofy 1", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testComparison()
    {
      try
      {
        mMeta.setString("<? if('one < two') ?>Correct!<? else ?>Wrong!<? endif ?>");
        assertEquals("Correct!", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testRegexExists()
    {
      try
      {
        mMeta.setString("<? if reExists('^on(.*)') ?>Correct!<? else ?>Wrong!<? endif ?>");
        assertEquals("Correct!", mMeta.evaluate(mParams));
        mMeta.setString("<? if reExists('^off(.*)') ?>Correct!<? else ?>Wrong!<? endif ?>");
        assertEquals("Wrong!", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testAny()
    {
      try
      {
        mMeta.setString("<? if any('apple, two') ?>Correct<? else ?>Wrong<? endif ?>");
        assertEquals("Correct", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testMultiExists()
    {
      try
      {
        mMeta.setString("<? if exists('two, apple') ?>Wrong<? else ?>Correct<? endif ?>");
        assertEquals("Correct", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testObjectAccessor()
    {
      Thing thingOne = new Thing();
      mParams.put("thingOne", thingOne);

      try
      {
        mMeta.setString("<? if exists('thingOne') ?><? value('thingOne.one') ?><? endif ?>");
        assertEquals("'one'", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }

    @Test
    public void testComment()
    {
      try
      {
        mMeta.setString("A<? comment ?>B<? if exists('thing') ?>C<? else ?>D<? endif ?>E<? endcomment ?>Z");
        assertEquals("AZ", mMeta.evaluate(mParams));
      }
      catch (Exception ex)
      {
        System.out.println(ex.toString());
        assertTrue(false);
      }
    }
}

