/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.commands;

import java.util.List;

import com.drakeapplications.metastring4j.MetaStringCommand;
import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringEvalException;

/**
 * A conditional command sent to the delegate for
 * processing.
 * @since 1.0
 */
public class DelegatedCommand extends MetaStringCommand
{
    protected String mName;

    /**
     * Construct a Delegated command
     * @since 1.0
     */
    public DelegatedCommand(String inName)
    {
      super();
      mName = inName;
    }

    /**
     * Determines wither a tag condition evaluated by a command is
     * met or not in the current evaluation context.
     *
     * @since 1.0
     * @param inContext the evaluation context
     * @param inArguments the conditional arguments
     * @return true if the condition is met
     * @throws MetaStringEvalException
     */
    @Override
    public boolean isCommandConditionMet(MetaStringContext inContext,
                                         List<String> inArguments)
                      throws MetaStringEvalException
    {
      boolean met = false;

      if (inContext.hasDelegate())
      {
        met = inContext.getDelegate().isCommandConditionMet(mName, inContext, inArguments);
      }
      else
      {
        throw new MetaStringEvalException("Unknown conditional command: " + mName, null);
      }
      return met;
    }

}

