/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.commands;

import java.util.List;

import com.drakeapplications.metastring4j.MetaStringCommand;
import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringEvalException;

/**
 * Conditional command that evaluates whether a
 * given argument exists.
 * @since 1.0
 */
public class AnyCommand extends MetaStringCommand
{
    /**
     * Construct an Any command
     * @since 1.0
     */
    public AnyCommand()
    {
      super();
    }

    /**
     * Determines wither a tag condition evaluated by a command is
     * met or not in the current evaluation context.
     *
     * @since 1.0
     * @param inContext the evaluation context
     * @param inArguments the conditional arguments
     * @return true if the condition is met
     * @throws MetaStringEvalException
     */
    @Override
    public boolean isCommandConditionMet(MetaStringContext inContext,
                                         List<String> inArguments)
                      throws MetaStringEvalException
    {
      boolean met = false;
      for (String paramName : inArguments)
      {
        met = inContext.hasValue(paramName);
        if (met)
        {
          break;
        }
      }
      return met;
    }

}

