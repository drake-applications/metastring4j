/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

/**
 * Children of this class define the insertion behavior when
 * a String parameter is written to a MetaString.
 *
 * @since 1.0
 */
public abstract class MetaStringFormatter
{
    protected String mDelimiter = null;
    protected String mNullValue = null;

    /**
     * Default constructor.
     * @since 1.0
     */
    protected MetaStringFormatter()
    {
      mDelimiter = null;
      mNullValue = null;
    }

    /**
     * Get the delimeter between elements in a list.
     * @since 1.0
     * @return list element delimiter
     */
    public String getDelimiter()
    {
      return mDelimiter;
    }

    /**
     * Get the value that represents the NULL value.
     * @since 1.0
     * @return null value
     */
    public String getNullValue()
    {
      return mNullValue;
    }

    /**
     * Escape a string for insertion into a MetaString.
     * @since 1.0
     * @param inString the string to escape
     * @return escaped string
     */
    public String escapeString(String inString)
    {
      return inString;
    }
}

