/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.Iterator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drakeapplications.metastring4j.MetaStringBlock;
import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringEvalException;
import com.drakeapplications.metastring4j.MetaStringFormatter;
import com.drakeapplications.metastring4j.MetaStringTag;

/**
 * A type of MetaStringTag that executes control logic.
 * @since 1.0
 */
public abstract class ControlTag extends MetaStringTag
{
    /**
     * Construct a MetaStringTag.
     * @since 1.0
     *
     * @param inStart whether the tag is the start of a block
     * @param inEnd whether the tag is the end of a block
     * @param inArguments zero or more inputs to the tag/command
     */
    protected ControlTag(boolean inStart,
                         boolean inEnd,
                         List<String> inArguments)
    {
      super(inStart, inEnd, inArguments);
    }

    /**
     * Utility function to write dependent blocks into the
     * MetaString output.
     * @since 1.0
     *
     * @param ioBuilder the meta string being built
     * @param inContext the evaulation context
     * @param inFormatter the meta string formatter
     * @throws MetaStringEvalException
     */
    protected boolean includeBlocks(StringBuilder ioBuilder,
                                    MetaStringContext inContext,
                                    MetaStringFormatter inFormatter)
                      throws MetaStringEvalException
    {
      boolean evaluated = true;
      for (MetaStringBlock block : mBlocks)
      {
        block.evaluate(ioBuilder, inContext, inFormatter);
      }
      return evaluated;
    }

    /**
     * Utiity function that determines whether all the given iterators
     * have a next value.
     * @since 1.0
     *
     * @param inIteratorMap a named map of iterators to check
     * @return true if all iterators have a next value
     */
    protected boolean iteratorsHaveNext(HashMap<String, Iterator<Object>> inIteratorMap)
    {
      boolean hasNext = true;
      for (Map.Entry<String, Iterator<Object>> entry : inIteratorMap.entrySet())
      {
        hasNext = entry.getValue().hasNext();
        if (!hasNext)
          break;
      }
      return hasNext;
    }
}

