/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

import java.text.ParseException;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Deque;
import java.util.ArrayDeque;

import com.drakeapplications.metastring4j.tags.ConditionalTag;
import com.drakeapplications.metastring4j.tags.ForeachTag;
import com.drakeapplications.metastring4j.tags.NonParameterized;
import com.drakeapplications.metastring4j.tags.PairedControl;
import com.drakeapplications.metastring4j.tags.PairedControlEnder;

/**
 * Metamorphic, parameterized string class capable of building
 * different Strings based on evaluated parameters.
 * @since 1.0
 */
public class MetaString
{
    // MetaString Class Members

    private MetaStringLexicon mLexicon = null;
    private MetaStringFormatter mFormatter = null;
    private MetaStringDelegate mDelegate = null;
    private int mBufferSizeHint = 0;
    private int mParsedStringLength = 0;

    private ArrayList<MetaStringBlock> mBlocks = new ArrayList<>();

    /**
     * Construct an empty MetaString that follows the given syntax
     * and uses the given formatter, optionally assisted by a delegate.
     *
     * @since 1.0
     * @param inLexicon the MetaString syntax
     * @param inFormatter the String formatter for parameter evaluation
     * @param inDelegate the delegate for obtaining sub-MetaStrings or null
     * @param inSizeHint how large a StringBuffer to preallocate when evaluating
     */
    public MetaString(MetaStringLexicon inLexicon,
                      MetaStringFormatter inFormatter,
                      MetaStringDelegate inDelegate,
                      int inSizeHint)
    {
      mLexicon = inLexicon;
      mFormatter = inFormatter;
      mDelegate = inDelegate;
      mBufferSizeHint = inSizeHint;
    }

    /**
     * Assign an unparsed String to the MetaString.
     *
     * @since 1.0
     * @param inRawString the unparsed meta string
     * @throws ParseException
     */
    public void setString(final String inRawString) throws ParseException
    {
      mBlocks.clear();
      parseString(inRawString);
    }

    /**
     * Evaluate this MetaString with the given set of parameters.
     *
     * @since 1.0
     * @param inParameters a map of parameter values
     * @return the evaluated string
     */
    public String evaluate(final Map<String, Object> inParameters) throws MetaStringEvalException
    {
      Map<String, Object> overrides = new HashMap<>();
      return evaluate(inParameters, overrides);
    }
    
    /**
     * Evaluate this MetaString with the given set of parameters
     * and overrides.
     *
     * @since 1.0
     * @param inParameters a map of parameter values
     * @param inOverrideParams a map of higher priority parameter values
     * @return the evaluated string
     */
    public String evaluate(final Map<String,Object> inParameters,
                           final Map<String,Object> inOverrideParams) throws MetaStringEvalException
    {
      MetaStringContext context = new MetaStringContext(inParameters, inOverrideParams, mDelegate);
      return evaluate(context);
    }

    /**
     * Evaluate this MetaString with the given context of
     * parameters.
     *
     * @since 1.0
     * @param inContext the evaluation/parameterization context
     * @return the evaluated string
     */
    public String evaluate(MetaStringContext inContext) throws MetaStringEvalException
    {
      int bufferSize = 0;

      if (mBufferSizeHint > 0)
      {
        bufferSize = mBufferSizeHint;
      }
      else
      {
        bufferSize = 2 * mParsedStringLength;
      }

      StringBuilder builder = new StringBuilder(bufferSize);

      try
      {
        for (MetaStringBlock block : mBlocks)
        {
          block.evaluate(builder, inContext, mFormatter);
        }
      }
      catch (Exception ex)
      {
        throw new MetaStringEvalException(ex.getMessage(), ex);
      }

      return builder.toString();
    }

    /**
     * Internal method to parse the input string into MetaString blocks.
     *
     * @since 1.0
     * @param inRawString the string to parse
     * @throws ParseException
     */
    private void parseString(final String inRawString) throws ParseException
    {
      MetaStringBlock block = null;
      MutableInteger index = new MutableInteger(0);
      int stringLength = inRawString.length();
      int scanLength = stringLength;

      mParsedStringLength = stringLength;

      Deque<String> operationStack = new ArrayDeque<>();

      do
      {
        block = readNextBlock(inRawString, index, scanLength, null, operationStack);
        if (block != null)
        {
          mBlocks.add(block);
        }
        scanLength = stringLength - index.intValue();
      }
      while (index.intValue() < stringLength);

      if (!operationStack.isEmpty())
      {
        String lastOp = operationStack.peek();
        throw new ParseException("Unterminated tag: " + lastOp, index.intValue());
      }
    }

    /**
     * Internal method to parse the next block of MetaString
     * from the assigned raw String.
     *
     * @since 1.0
     * @param inString the raw String to parse
     * @param ioIndex the current read index, updated after reading the block
     * @param inScanLength the number of characters to consider
     * @param inParentBlock the block to which subordinate tags will be added
     * @param inOpStack the stack showing the nesting of start/end tags in progress
     * @return the next MetaString block
     * @throws ParseException
     */
    private MetaStringBlock readNextBlock(final String inString,
                                          MutableInteger ioIndex,
                                          int inScanLength,
                                          MetaStringBlock inParentBlock,
                                          Deque<String> inOpStack) throws ParseException
    {
      MetaStringBlock block = null;
      MetaStringBlock parent = null;
      MetaStringBlock newBlock = null;
      MetaStringTag tag = null;

      int start = 0;
      int startLength = 0;
      int stringLength = inString.length();

      if (inScanLength > 0)
      {
        start = inString.indexOf(mLexicon.getStartTag(), ioIndex.intValue());
        if (start < 0)
        {
          block = new MetaStringBlock(inString.substring(ioIndex.intValue(), ioIndex.intValue() + inScanLength));
          ioIndex.add(inScanLength);
        }
        else if (start > ioIndex.intValue())
        {
          startLength = start - ioIndex.intValue();
          block = new MetaStringBlock(inString.substring(ioIndex.intValue(), ioIndex.intValue() + startLength));
          ioIndex.add(startLength);
        }
        else
        {
          tag = readNextTag(inString, ioIndex, inOpStack);
          startLength = stringLength - ioIndex.intValue();
 
          if (tag.isEndOfBlock())
          {
            if (inParentBlock != null)
            {
              inParentBlock.addTag(tag);
            }
            else
            {
              throw new ParseException("MetaString Parse Error: Closing tag found before opening tag.", ioIndex.intValue());
            }
          }

          if (tag.isStartOfBlock())
          {
            block = new MetaStringBlock();
            if (!tag.isEndOfBlock())
            {
              block.addTag(tag);
              parent = block;
            }
            else
            {
              parent = inParentBlock;
            }

            do
            {
              newBlock = readNextBlock(inString, ioIndex, startLength, parent, inOpStack);
              if (newBlock != null)
              {
                tag.addBlock(newBlock);
              }
              startLength = stringLength - ioIndex.intValue();
            }
            while (newBlock != null);
          }
 
          if (!tag.isStartOfBlock() && !tag.isEndOfBlock())
          {
            block = new MetaStringBlock();
            block.addTag(tag);
          }
        }
        if (block != null && block.isEmpty())
        {
          block = null;
        }
      }
      return block;
    }

    /**
     * Parse the next tag from the raw input string.
     * Note that tags have blocks but not other tags.
     *
     * @since 1.0
     * @param inString the raw string being parsed
     * @param ioIndex where to start reading (is updated when tag is read)
     * @param inOpStack the stack showing the nesting of start/end tags in progress
     * @return the next MetaStringTag
     * @throws ParseException
     * @todo refactor into smaller subroutines if possible
     */
    private MetaStringTag readNextTag(final String inString,
                                      MutableInteger ioIndex,
                                      Deque<String> inOpStack) throws ParseException
    {
      String startTag = mLexicon.getStartTag();
      String endTag = mLexicon.getEndTag();
      String startFunction = mLexicon.getStartFunction();

      String endWord = mLexicon.getEnd();
      String notWord = mLexicon.getNot();
      String delimitedWord = mLexicon.getDelimited();
      String altQuote = mLexicon.getAltQuote();

      int tagMarkerLength = startTag.length();

      MetaStringTag tag = null;
      MetaStringCommand command = null;

      int tagStartPosition = inString.indexOf(startTag, ioIndex.intValue());
      int tagEndPosition = inString.indexOf(endTag, ioIndex.intValue());
      int tagLength = 0;

      int searchPosition = 0;
      int searchLength = 0;

      int operatorStartPosition = 0;
      int operatorEndPosition = 0;

      String tagString = null;
      String tagType = null;

      if (tagStartPosition >= 0)
      {
        if (tagEndPosition < 0)
        {
          throw new ParseException("MetaString Parse Error: Missing end tag " + endTag, ioIndex.intValue());
        }
        if (tagStartPosition >= tagEndPosition)
        {
          throw new ParseException("MetaString Parse Error: " + endTag + " precedes " + startTag, ioIndex.intValue());
        }

        tagEndPosition += tagMarkerLength;
        tagLength = tagEndPosition - tagStartPosition;

        searchPosition = tagStartPosition;
        searchLength = tagLength;

        tagString = inString.substring(searchPosition, searchPosition + searchLength);

        operatorStartPosition = tagMarkerLength;
        operatorEndPosition = tagString.indexOf(startFunction);

        if (operatorEndPosition < 0)
        {
          operatorEndPosition = tagLength - tagMarkerLength;
        }

        tagType = tagString.substring(operatorStartPosition, operatorEndPosition).trim();

        if (tagType == null || tagType.length() == 0)
        {
          throw new ParseException("Empty tag.", ioIndex.intValue());
        }

        // Now that we have a tag, finish it out with its parts, arguments, etc.
        if (tagType.startsWith(endWord))
        {
          tagType = tagType.substring(0, endWord.length()) + " " + tagType.substring(endWord.length());
        }

        String[] parts = tagType.split(" ");
        tag = mLexicon.newTag(parts[0].trim());

        // Make sure we got a valid tag
        if (tag == null)
        {
          throw new ParseException("Unknown tag '" + tagType + "'", ioIndex.intValue());
        }

        // Handle multi-part tags
        if (parts.length > 1)
        {
          if (tag instanceof PairedControlEnder)
          {
            // Obtain and set the type of end tag
            PairedControlEnder pceTag = (PairedControlEnder)tag;
            pceTag.setType(parts[1].trim());
          }
          else if (tag instanceof ConditionalTag)
          {
            // Handle condition negation and/or condition command
            ConditionalTag conditionalTag = (ConditionalTag)tag;
            String secondary = parts[1];
            if (secondary.equals(notWord))
            {
              conditionalTag.setNegated(true);
              if (parts.length > 2)
              {
                secondary = parts[2];
              }
            }
            if (secondary.length() > 0)
            {
              command = readTagCommand(tagString);
              if (command != null)
              {
                conditionalTag.setCommand(command);
              }
            }
          }
          else if (tag instanceof ForeachTag)
          {
            // Handle delimited option
            ForeachTag feTag = (ForeachTag)tag;
            String secondary = parts[1];
            if (secondary.equals(delimitedWord))
            {
              if (parts.length > 2)
              {
                StringBuilder delimBuilder = new StringBuilder().append(parts[2]);
                for (int i = 3; i < parts.length; i++)
                {
                  delimBuilder.append(" ");
                  delimBuilder.append(parts[i]);
                }
                String customDelimiter = delimBuilder.toString();
                if (customDelimiter.startsWith(altQuote) && customDelimiter.endsWith(altQuote) && customDelimiter.length() > 1)
                {
                  customDelimiter = customDelimiter.substring(1, customDelimiter.length() - 1);
                }
                feTag.setDelimiter(customDelimiter);
              }
              else
              {
                feTag.setDelimiter(mFormatter.getDelimiter());
              }
            }
          }
        }

        // Handle operation stack validation
        if (tag instanceof PairedControlEnder)
        {
          PairedControlEnder pceTag = (PairedControlEnder)tag;
          validateOpStackPop(inOpStack, pceTag.getType(), endWord, ioIndex.intValue());
        }
        else if (tag instanceof PairedControl)
        {
          inOpStack.push(tag.getName());
        }

        // Handle tag arguments
        if (!(tag instanceof NonParameterized))
        {
          tag.setArguments(readTagParameters(tagString.substring(operatorEndPosition)));
        }

        // Validate the tag
        StringBuilder errorMessage = new StringBuilder();
        if (!tag.validate(errorMessage))
        {
          errorMessage.append(" (tag type = '").append(tagType).append("')");
          throw new ParseException(errorMessage.toString(), ioIndex.intValue());
        }

        // Move on in the string past this tag
        ioIndex.add(tagLength);
      }

      return tag;
    }

    /**
     * Utility function to look for a command related to the tag,
     * for example the word "exists" in "if exists(...)".
     *
     * @since 1.0
     * @param inTagString the String containing the overall tag
     * @return the parsed command or null
     */
    private MetaStringCommand readTagCommand(final String inTagString)
    {
      MetaStringCommand tagCommand = null;
      String command = null;

      String ifWord = mLexicon.getIf();
      String notWord = mLexicon.getNot();
      String startFunction = mLexicon.getStartFunction();

      int cmdStart = 0;

      if (inTagString.indexOf(" " + notWord + " ") >= 0)
      {
        cmdStart = inTagString.indexOf(notWord) + notWord.length() + 1;
      }
      else
      {
        cmdStart = inTagString.indexOf(ifWord) + ifWord.length() + 1;
      }
      int cmdEnd = inTagString.indexOf(startFunction);

      if (cmdStart >= 0 && cmdEnd >= 0 && cmdStart < cmdEnd)
      {
        command = inTagString.substring(cmdStart, cmdEnd);

        if (command.length() > 0)
        {
          tagCommand = mLexicon.newCommand(command);
        }
      }
      return tagCommand;
    }

    /**
     * Utility function to read the parameters from the tag String,
     * the part that comes between the parentheses (or equivalent
     * in the current lexicon.
     *
     * @since 1.0
     * @param inTagString the String containing the overall tag
     * @return the list of parsed parameters in the tag
     * @throws ParseException
     */
    private ArrayList<String> readTagParameters(final String inTagString) throws ParseException
    {
      ArrayList<String> parameterList = new ArrayList<>();
      String quote = mLexicon.getQuote();
      String altQuote = mLexicon.getAltQuote();

      String parameter;
      int quoteOpenPosition = inTagString.indexOf(quote);
      int quoteClosePosition = inTagString.lastIndexOf(quote);

      if (quoteOpenPosition < 0)
      {
        quoteOpenPosition = inTagString.indexOf(altQuote);
        quoteClosePosition = inTagString.lastIndexOf(altQuote);
      }

      if (quoteOpenPosition >= 0 && quoteClosePosition >= 0 && quoteClosePosition > quoteOpenPosition)
      {
        parameter = inTagString.substring(quoteOpenPosition + 1, quoteClosePosition);
        String[] params = parameter.split("(,)|( )");

        for (String param : params)
        {
          String trimmedParam = param.trim();
          if (trimmedParam.length() > 0)
          {
            parameterList.add(trimmedParam);
          }
        }
      }
      else
      {
        throw new ParseException("MetaString Parse Error: Malformed tag - cannot read parameter: " + inTagString, 0);
      }

      return parameterList;
    }

    /**
     * Utility function to make sure the end tag we are parsing
     * matches the corresponding start tag.
     *
     * @since 1.0
     * @param inOpStack the stack showing the nesting of start/end tags in progress
     * @param inExpected the type of tag we expect to find on inOpStack
     * @param inEnd the word meaning "end in the current lexicon
     * @param inIndex where we are currently in the raw String to parse
     * @throws ParseException
     */
    public void validateOpStackPop(Deque<String> inOpStack, String inExpected, String inEnd, int inIndex) throws ParseException
    {
      if (inOpStack.isEmpty())
      {
        throw new ParseException("End tag found without start tag for '" + inExpected + "'", inIndex);
      }
      else
      {
        String found = inOpStack.pop();
        if (!found.equals(inExpected))
        {
          throw new ParseException("Mismatched end tag for '" + found + "' (got '" + inEnd + inExpected + "')", inIndex);
        }
      }
    }
}

