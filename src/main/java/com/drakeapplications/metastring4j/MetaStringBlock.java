/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

import java.util.ArrayList;

/**
 * This class represents a parsed block of MetaString.
 * @since 1.0
 */
public class MetaStringBlock
{
    private String mContent = null;
    private ArrayList<MetaStringTag> mTags = new ArrayList<>();

    /**
     * Default constructor builds an empty block.
     * @since 1.0
     */
    public MetaStringBlock()
    {

    }

    /**
     * Construct a block having some content that is part
     * of the MetaString.
     *
     * @since 1.0
     * @param inContent a unit of MetaString
     */
    public MetaStringBlock(String inContent)
    {
      mContent = inContent;
    }

    /**
     * Add a tag that is part of the current block
     *
     * @since 1.0
     * @param inTag the tag to add
     */
    public void addTag(MetaStringTag inTag)
    {
      mTags.add(inTag);
    }

    /**
     * Determine if this block is empty.
     *
     * @since 1.0
     * @return true if empty
     */
    public boolean isEmpty()
    {
      return ((mContent == null || mContent.equals("")) && mTags.isEmpty());
    }

    /**
     * Evaluate a block by adding its content to the resulting
     * StringBuilder.
     *
     * @since 1.0
     * @param ioBuilder the StringBuilder containing evaluated MetaString
     * @param inContext the evaluation/parameterization context
     * @param inFormatter the String formatter
     */
    public boolean evaluate(StringBuilder ioBuilder, MetaStringContext inContext, MetaStringFormatter inFormatter)
    {
      boolean evaluated = false;

      if (mContent != null && mContent.length() > 0)
      {
        ioBuilder.append(mContent);
      }

      for (MetaStringTag tag : mTags)
      {
        if (!(evaluated && tag.isEndOfBlock()))
        {
          evaluated = tag.evaluate(ioBuilder, inContext, inFormatter);
        }
      }

      return evaluated;
    }
}

