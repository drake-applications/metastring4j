/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

/**
 * This class defines the insertion behavior when String
 * parameters are written to a MetaString by applying SQL
 * syntax and rules.
 *
 * @since 1.0
 */
public class MetaStringSqlFormatter extends MetaStringFormatter
{
    /**
     * Default constructor sets the delimter.
     * @since 1.0
     */
    public MetaStringSqlFormatter()
    {
      mDelimiter = ", ";
    }

    /**
     * Ensure that the given string is properly escaped as a
     * valid SQL string, and avoid SQL injection.
     * @since 1.0
     *
     * @param inString the string to escape
     * @return the escaped string
     */
    @Override
    public String escapeString(String inString)
    {
      StringBuilder builder = new StringBuilder();
      return builder.append("'").append(inString.replace("'", "''")).append("'").toString();
    }
}

