package com.drakeapplications.metastring4j;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.drakeapplications.metastring4j.MetaStringLexicon;

/**
 * Unit test for MetaStringLexicon
 */
public class MetaStringEmptyLexiconTest
{
    /**
     * Create the test case
     */
    public MetaStringEmptyLexiconTest()
    {
    }


    @Test
    public void testLexicon()
    {
      MetaStringLexicon lexicon = new EmptyCustomLexicon();
      assertNotNull(lexicon);
      assertNull(lexicon.getStartTag());
      assertNull(lexicon.getEndTag());
      assertNull(lexicon.getStartFunction());
      assertNull(lexicon.getEndFunction());
      assertNull(lexicon.getQuote());
      assertNull(lexicon.getAltQuote());
      assertNull(lexicon.getIf());
      assertNull(lexicon.getElse());
      assertNull(lexicon.getElseIf());
      assertNull(lexicon.getEnd());
      assertNull(lexicon.getNot());
      assertNull(lexicon.getExists());
      assertNull(lexicon.getRegexExists());
      assertNull(lexicon.getLiteral());
      assertNull(lexicon.getValue());
      assertNull(lexicon.getForeach());
      assertNull(lexicon.getForall());
      assertNull(lexicon.getComment());
      assertNull(lexicon.getDefault());
      assertNull(lexicon.getSubMeta());
      assertNull(lexicon.getAny());
      assertNull(lexicon.getDelimited());
    }
 
}
