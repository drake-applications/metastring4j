/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;

import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringFormatter;
import com.drakeapplications.metastring4j.MetaStringEvalException;

/**
 * A ControlTag that executes repetitions.
 * @since 1.0
 */
public class ForeachTag extends ControlTag implements PairedControl
{
    protected String mDelimiter = null;

    /**
     * Construct an empty Foreach tag.
     * @since 1.0
     */
    public ForeachTag()
    {
      super(true, false, null);
    }

    /**
     * Construct a Foreach tag.
     * @since 1.0
     *
     * @param inArguments zero or more inputs to the tag/command
     */
    public ForeachTag(List<String> inArguments)
    {
      super(true, false, inArguments);
    }

    /**
     * Set a delimiter between evaluation iterations.
     * @since 1.0
     *
     * @param inDelimiter the delimiter that goes between iterations
     */
    public void setDelimiter(String inDelimiter)
    {
      mDelimiter = inDelimiter;
    }

    /**
     * Evaluate this tag, appending the result into the StringBuilder.
     * @since 1.0
     *
     * @param ioBuilder the string builder containing the result string
     * @param inContext the evaluation/parameter context
     * @param inFormatter the string formatter for the output
     * @return whether an evaluation was performed or not
     *
     */
    @Override
    public boolean evaluate(StringBuilder ioBuilder,
                            MetaStringContext inContext,
                            MetaStringFormatter inFormatter)
                   throws MetaStringEvalException
    {
      boolean evaluated = false;
      boolean conditionMet = false;

      boolean hasValues = true;
      boolean areCollections = true;

      for (String param : mArguments)
      {
        hasValues = inContext.hasValue(param);
        if (!hasValues)
          break;

        areCollections = areCollections && inContext.getValue(param) instanceof Collection;
      }

      if (hasValues)
      {
        if (areCollections)
        {
          HashMap<String, Iterator<Object>> iterators = new HashMap<>();
          for (String param : mArguments)
          {
            Collection collection = (Collection)inContext.getValue(param);
            iterators.put(param, collection.iterator());
          }

          boolean isFirst = true;
          while (iteratorsHaveNext(iterators))
          {
            for (String param : mArguments)
            {
              Object curValue = null;
              Iterator<Object> iterator = iterators.get(param);
              if (iterator.hasNext())
              {
                curValue = iterator.next();
              }
              else
              {
                curValue = inFormatter.getNullValue();
              }
              inContext.setVariable(param, curValue);
            }

            if (!isFirst && mDelimiter != null)
            {
              // Add delimiter, if specified
              ioBuilder.append(mDelimiter);
            }

            includeBlocks(ioBuilder, inContext, inFormatter);

            isFirst = false;
          }
          for (String param : mArguments)
          {
            inContext.removeVariable(param);
          }
          evaluated = true;
        }
        else
        {
          conditionMet = true;
        }
      }

      if (conditionMet)
      {
        evaluated = includeBlocks(ioBuilder, inContext, inFormatter);
      }

      return evaluated;
    }
}

