/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

/**
 * Builder to create MetaStrings having a common formatter,
 * lexicon, and delegate.
 * @since 1.0
 */
public class MetaStringBuilder
{
    private MetaStringFormatter mFormatter = null;
    private MetaStringLexicon mLexicon = null;
    private MetaStringDelegate mDelegate = null;

    /**
     * Default constructor
     * @since 1.0
     */
    public MetaStringBuilder()
    {
      mFormatter = null;
      mLexicon = null;
    }

    /**
     * Set the formatter to assign to new MetaStrings.
     * @since 1.0
     * @param inFormatter the string formatter to use
     */
    public MetaStringBuilder setFormatter(MetaStringFormatter inFormatter)
    {
      mFormatter = inFormatter;
      return this;
    }

    /**
     * Set the lexicon to use for parsing/evaluating new MetaStrings.
     * @since 1.0
     * @param inLexicon the lexicon to use
     */
    public MetaStringBuilder setLexicon(MetaStringLexicon inLexicon)
    {
      mLexicon = inLexicon;
      return this;
    }

    /**
     * Set the delegate new MetaStrings should use (optional, as
     * these are only required for sub-MetaString acquisition).
     * @since 1.0
     * @param inDelegate the delegate
     */
    public MetaStringBuilder setDelegate(MetaStringDelegate inDelegate)
    {
      mDelegate = inDelegate;
      return this;
    }

    /**
     * Get the formatter currently in use.
     * @since 1.0
     * @return the formatter in use
     */
    public MetaStringFormatter getFormatter()
    {
      return mFormatter; 
    }

    /**
     * Get the lexicon currently in use.
     * @since 1.0
     * @return the lexicon in use
     */
    public MetaStringLexicon getLexicon()
    {
      return mLexicon;
    }

    /**
     * Get the current delegate.
     * @since 1.0
     * @return the current delegate
     */
    public MetaStringDelegate getDelegate()
    {
      return mDelegate;
    }

    /**
     * Create a new, empty MetaString referencing the defined
     * lexicon, formatter, and delegate.
     * @since 1.0
     * @return a new, empty MetaString
     */
    public MetaString buildMetaString()
    {
      // Uses the default size hint.
      return buildMetaString(0);
    }

    /**
     * Create a new, empty MetaString referencing the defined
     * lexicon, formatter, and delegate, using the supplied, 
     * overridden default string buffer size for evaluations
     * of the new MetaString.
     * @since 1.0
     * @return a new, empty MetaString
     */
    public MetaString buildMetaString(int inSizeHint)
    {
      if (mLexicon == null)
      {
        mLexicon = new MetaStringDefaultLexicon();
      }
      if (mFormatter == null)
      {
        mFormatter = new MetaStringDefaultFormatter();
      }
      return new MetaString(mLexicon, mFormatter, mDelegate, inSizeHint);
    }
}

