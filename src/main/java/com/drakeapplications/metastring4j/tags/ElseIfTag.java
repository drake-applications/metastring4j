/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.List;

import com.drakeapplications.metastring4j.MetaStringCommand;

/**
 * A type of ConditionalTag that checks whether a
 * different condition is met.
 * @since 1.0
 */
public class ElseIfTag extends ConditionalTag
{
    /**
     * Construct an empty ElseIfTag.
     * @since 1.0
     */
    public ElseIfTag()
    {
      super(true, false, false, null, null);
    }

    /**
     * Construct an ElseIf MetaStringTag
     * @since 1.0
     *
     * @param inNegated whether this is an 'if not' tag or not
     * @param inCommand an operation to perform on tag parameters
     * @param inArguments zero or more inputs to the tag/command
     */
    public ElseIfTag(boolean inNegated,
                     MetaStringCommand inCommand,
                     List<String> inArguments)
    {
      super(true, false, inNegated, inCommand, inArguments);
    }
}

