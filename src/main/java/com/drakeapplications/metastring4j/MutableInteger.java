/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j;

/**
 * This class defines a simple Integer type class that
 * can reassign its value.
 *
 * It is used internally by MetaString.
 */

public class MutableInteger
{
    private int mValue = 0;

    /**
     * Default contructor
     * @since 1.0
     */
    public MutableInteger()
    {
      mValue = 0;
    }

    /**
     * Constructor that accepts an int.
     * @since 1.0
     *
     * @param inInteger Initial value
     */
    public MutableInteger(int inInteger)
    {
      mValue = inInteger;
    }

    /**
     * Constructor that accepts an Integer.
     * @since 1.0
     *
     * @param inInteger Initial value
     */
    public MutableInteger(Integer inInteger)
    {
      mValue = inInteger.intValue();
    }

    /**
     * Get the current value as an int.
     * @since 1.0
     *
     * @returns current value
     */
    public int intValue()
    {
      return mValue;
    }

    /**
     * Set the value of the MutableInteger from an int.
     * @since 1.0
     *
     * @param inInteger
     * @return this
     */
    public MutableInteger setValue(int inInteger)
    {
      mValue = inInteger;
      return this;
    }

    /**
     * Set the value of the MutableInteger from an Integer.
     * @since 1.0
     *
     * @param inInteger
     * @return this
     */
    public MutableInteger setValue(Integer inInteger)
    {
      mValue = inInteger.intValue();
      return this;
    }

    /**
     * Add an int amount to the MutableInteger.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger add(int amount)
    {
      mValue += amount;
      return this;
    }

    /**
     * Add an Integer amount to the MutableInteger.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger add(Integer amount)
    {
      mValue += amount.intValue();
      return this;
    }

    /**
     * Subtract an int amount from the MutableInteger.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger subtract(int amount)
    {
      mValue -= amount;
      return this;
    }

    /**
     * Subtract an Integer amount from the MutableInteger.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger subtract(Integer amount)
    {
      mValue -= amount.intValue();
      return this;
    }

    /**
     * Muliply the MutableInteger by the given int amount.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger multiply(int amount)
    {
      mValue *= amount;
      return this;
    }

    /**
     * Muliply the MutableInteger by the given Integer amount.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger multiply(Integer amount)
    {
      mValue *= amount.intValue();
      return this;
    }

    /**
     * Divide the MutableInteger by the given int amount.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger divide(int amount)
    {
      mValue /= amount;
      return this;
    }

    /**
     * Divide the MutableInteger by the given Integer amount.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger divide(Integer amount)
    {
      mValue /= amount.intValue();
      return this;
    }

    /**
     * Get the remainder when dividing the MutableInteger by
     * the given int amount.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger modulus(int amount)
    {
      mValue = mValue % amount;
      return this;
    }

    /**
     * Get the remainder when dividing the MutableInteger by
     * the given Integer amount.
     * @since 1.0
     *
     * @param amount
     * @return this
     */
    public MutableInteger modulus(Integer amount)
    {
      mValue = mValue % amount.intValue();
      return this;
    }

    /**
     * Convert the MutableInteger to a String.
     * @since 1.0
     *
     * @return String representation of the MutableInteger
     */
    public String toString()
    {
      Integer localValue = Integer.valueOf(mValue);
      return localValue.toString();
    }
}
