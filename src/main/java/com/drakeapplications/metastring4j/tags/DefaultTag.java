/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.List;

import com.drakeapplications.metastring4j.MetaStringContext;
import com.drakeapplications.metastring4j.MetaStringFormatter;
import com.drakeapplications.metastring4j.MetaStringEvalException;

/**
 * A MetaStringTag that sets a default argument value.
 * @since 1.0
 */
public class DefaultTag extends ControlTag implements PairedControl
{
    /**
     * Construct a Default tag.
     * @since 1.0
     */
    public DefaultTag()
    {
      super(true, false, null);
    }

    /**
     * Construct a Default tag.
     * @since 1.0
     *
     * @param inArguments zero or more inputs to the tag/command
     */
    public DefaultTag(List<String> inArguments)
    {
      super(true, false, inArguments);
    }

    /**
     * @Inherit
     */
    @Override
    public boolean validate(StringBuilder outMessage)
    {
      boolean valid = true;
      if (mArguments.size() != 1)
      {
        outMessage.append("Incorrect number of arguments");
        valid = false;
      }
      return valid;
    }

    /**
     * Evaluate this tag, appending the result into the StringBuilder.
     * @since 1.0
     *
     * @param ioBuilder the string builder containing the result string
     * @param inContext the evaluation/parameter context
     * @param inFormatter the string formatter for the output
     * @return whether an evaluation was performed or not
     *
     */
    @Override
    public boolean evaluate(StringBuilder ioBuilder,
                            MetaStringContext inContext,
                            MetaStringFormatter inFormatter)
                   throws MetaStringEvalException
    {
      boolean evaluated = true;

      StringBuilder defBuilder = new StringBuilder();
      includeBlocks(defBuilder, inContext, inFormatter);
      inContext.setDefault(mArguments.get(0), deduceObject(defBuilder.toString()));

      return evaluated;
    }
}

