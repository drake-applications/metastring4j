package com.drakeapplications.metastring4j;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.drakeapplications.metastring4j.MutableInteger;

/**
 * Unit test for MetaString
 */
public class MutableIntegerTest
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MutableIntegerTest()
    {
    }

    /**
     * Rigourous Test :-)
     */
    @Test
    public void testOperations()
    {
        MutableInteger integer = new MutableInteger();
        assertEquals(3, integer.add(3).intValue());
        assertEquals(-1, integer.subtract(4).intValue());
        assertEquals(10, integer.multiply(-10).intValue());
        assertEquals(5, integer.divide(2).intValue());
        assertEquals(1, integer.modulus(2).intValue());
    }
}
