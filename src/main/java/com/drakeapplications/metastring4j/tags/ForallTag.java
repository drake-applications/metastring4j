/*
 * Copyright (c) 2021 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.drakeapplications.metastring4j.tags;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * A ControlTag that executes repetitions.
 * @since 1.0
 */
public class ForallTag extends ForeachTag implements PairedControl
{
    /**
     * Construct an empty Forall tag.
     * @since 1.0
     */
    public ForallTag()
    {
      super();
    }

    /**
     * Construct a Forall tag.
     * @since 1.0
     *
     * @param inArguments zero or more inputs to the tag/command
     */
    public ForallTag(List<String> inArguments)
    {
      super(inArguments);
    }

    /**
     * Utiity function that determines whether the given iterators
     * have a next value.
     * @since 1.0
     *
     * @param inIteratorMap a named map of iterators to check
     * @return true if at least one iterator has a next value
     */
    @Override
    protected boolean iteratorsHaveNext(HashMap<String, Iterator<Object>> inIteratorMap)
    {
      boolean hasNext = false;
      for (Map.Entry<String, Iterator<Object>> entry : inIteratorMap.entrySet())
      {
        hasNext = entry.getValue().hasNext();
        if (hasNext)
          break;
      }
      return hasNext;
    }
}

