package com.drakeapplications.metastring4j;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.drakeapplications.metastring4j.MetaStringDefaultLexicon;

/**
 * Unit test for MetaStringLexicon
 */
public class MetaStringLexiconTest
{
    /**
     * Create the test case
     */
    public MetaStringLexiconTest()
    {
    }


    @Test
    public void testLexicon()
    {
      MetaStringDefaultLexicon lexicon = new MetaStringDefaultLexicon();
      assertNotNull(lexicon);
      assertEquals("<?", lexicon.getStartTag());
      assertEquals("?>", lexicon.getEndTag());
      assertEquals("(", lexicon.getStartFunction());
      assertEquals(")", lexicon.getEndFunction());
      assertEquals("\"", lexicon.getQuote());
      assertEquals("'", lexicon.getAltQuote());
      assertEquals("if", lexicon.getIf());
      assertEquals("else", lexicon.getElse());
      assertEquals("elseif", lexicon.getElseIf());
      assertEquals("end", lexicon.getEnd());
      assertEquals("not", lexicon.getNot());
      assertEquals("exists", lexicon.getExists());
      assertEquals("reExists", lexicon.getRegexExists());
      assertEquals("literal", lexicon.getLiteral());
      assertEquals("value", lexicon.getValue());
      assertEquals("foreach", lexicon.getForeach());
      assertEquals("forall", lexicon.getForall());
      assertEquals("comment", lexicon.getComment());
      assertEquals("default", lexicon.getDefault());
      assertEquals("sub", lexicon.getSubMeta());
      assertEquals("any", lexicon.getAny());
      assertEquals("delimited", lexicon.getDelimited());
    }
 
}
